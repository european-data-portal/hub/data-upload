# data-upload

The data-upload module enabled data pushes into the Hub via API.

## Setup

1. Install all of the following software
        
        * Java JDK >= 1.8
        * Git >= 2.17
  
2. Clone the directory and enter it
    
        git@gitlab.com:european-data-portal/data-upload.git
        
3. Edit the environment variables in the `Dockerfile` to your liking. Variables and their purpose are listed below:
   
    |Key|Description|Example|
    |:--- |:---|:---|
    |MONGO_DB_URI| The URL for the mongodb used by this service| mongodb://localhost:27017 |
    |MONGO_DB| The name for the database the files will be stored in  | fileDB |
    |HTTP_PORT| Port used to communicate with the API | 8080 | 
    |API_KEY| The API_key used to secure the connection in certain requests | 22921c47-0c78-424b-bac5-ccf4a9393123 |
    
## Run

Build the project by using the provided Maven wrapper. This ensures everyone this software is provided to can use the exact same version of the maven build tool.
The generated _fat-jar_ can then be found in the `target` directory.

* Linux
    
        ./mvn clean package
        java -jar target/data-upload-fat.jar

* Windows

        mvnw.cmd clean package
        java -jar target/data-upload-fat.jar
      
* Docker
    
        1. Start your docker daemon 
        2. Build the application as described in Windows or Linux
        3. Adjust the port number (`EXPOSE` in the `Dockerfile`)
        4. Build the image: `docker build -t data-upload .`
        5. Run the image, adjusting the port number as set in step 3: `docker run -i -p 8081:8081 data-upload`
        6. Configuration can be changed without rebuilding the image by overriding variables: `-e PORT=8086`



## API

A formal OpenAPI 3 specification can be found in the `src/main/resources/webroot/openapi.yaml` file.
A visually more appealing version is available at `{url}:{port}` once the application has been started.